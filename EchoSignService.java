package com.sap.eso.doccommon.esignature;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.sap.eso.odp.runtime.dynenum.IntegratedSystemTypeEnum;
import com.sap.eso.odp.runtime.dynenum.MailTypeEnum;
import com.sap.eso.webservice.echosign.api.ArrayOfString;
import com.sap.eso.webservice.echosign.api.CancelDocument;
import com.sap.eso.webservice.echosign.api.CancelDocumentResponse;
import com.sap.eso.webservice.echosign.api.CreateUser;
import com.sap.eso.webservice.echosign.api.CreateUserResponse;
import com.sap.eso.webservice.echosign.api.EchoSignDocumentService22;
import com.sap.eso.webservice.echosign.api.EchoSignDocumentService22PortType;
import com.sap.eso.webservice.echosign.api.GetDocumentInfo;
import com.sap.eso.webservice.echosign.api.GetDocumentInfoResponse;
import com.sap.eso.webservice.echosign.api.GetLatestDocument;
import com.sap.eso.webservice.echosign.api.GetLatestDocumentResponse;
import com.sap.eso.webservice.echosign.api.GetUserInfo;
import com.sap.eso.webservice.echosign.api.GetUserInfoResponse;
import com.sap.eso.webservice.echosign.api.MoveUsersToGroup;
import com.sap.eso.webservice.echosign.api.SendDocumentInteractive;
import com.sap.eso.webservice.echosign.api.SendDocumentInteractiveResponse;
import com.sap.eso.webservice.echosign.api.TestPing;
import com.sap.eso.webservice.echosign.api.dto.ArrayOfFileInfo;
import com.sap.eso.webservice.echosign.api.dto.CancelDocumentResult;
import com.sap.eso.webservice.echosign.api.dto.DocumentCreationInfo;
import com.sap.eso.webservice.echosign.api.dto.FileInfo;
import com.sap.eso.webservice.echosign.api.dto.Result;
import com.sap.eso.webservice.echosign.api.dto.SenderInfo;
import com.sap.eso.webservice.echosign.api.dto.SignatureFlow;
import com.sap.eso.webservice.echosign.api.dto.UserCreationInfo;
import com.sap.eso.webservice.echosign.api.dto12.SendDocumentInteractiveOptions;
import com.sap.eso.webservice.echosign.api.dto14.ArrayOfRecipientInfo;
import com.sap.eso.webservice.echosign.api.dto14.RecipientInfo;
import com.sap.eso.webservice.echosign.api.dto14.RecipientRole;
import com.sap.eso.webservice.echosign.api.dto15.UsersToMoveInfo;
import com.sap.eso.webservice.echosign.api.dto17.AgreementStatus;
import com.sap.eso.webservice.echosign.api.dto18.GetUserInfoOptions;
import com.sap.eso.webservice.echosign.api.dto18.GetUserInfoResult;
import com.sap.eso.webservice.echosign.api.dto18.GetUserInfoResultErrorCode;
import com.sap.eso.webservice.echosign.api.dto20.DocumentHistoryEvent;
import com.sap.eso.webservice.echosign.api.dto21.DocumentInfo;
import com.sap.eso.webservice.echosign.api.dto21.SendDocumentInteractiveResult;
import com.sap.odp.api.common.exception.ApplicationException;
import com.sap.odp.api.util.IapiSystemUtilities;
import com.sap.odp.common.db.ParentObjectReference;
import com.sap.odp.common.log.Log;
import com.sap.odp.common.platform.ConfigDatabase;
import com.sap.odp.common.platform.SessionContextIfc;
import com.sap.odp.common.types.Attachment;
import com.sap.odp.common.util.ClassUtils;
import com.sap.odp.comp.messaging.MessageBo;
import com.sap.odp.comp.messaging.MessagingComponent;
import com.sap.odp.doc.collection.SubCollnBo;
import com.sap.odp.doc.integration.IntegratedSystemConfigBo;
import com.sap.odp.doc.integration.IntegratedSystemConfigHome;
import com.sap.odp.util.EnvironmentIfc;
import com.sap.odp.util.StringUtil;

public class EchoSignService implements ESignatureProviderIfc {
	public static final String PROP_ECHOSIGN_URL = "contractgen.esignature_url";
	public static final String PROP_ECHOSIGN_TIMEOUT = "contractgen.esignature_timeout";
	public static final String PROP_ESIGN_TYPE = "contractgen.esignature_type";
	public static final String SYSTEM_APP_ID = "contractgen";
	public static final String INVALID_API_KEY = "INVALID_API_KEY";
	private static final QName ECHOSIGN_SERVICENAME = new QName("http://api.echosign", "EchoSignDocumentService22");

	EchoSignDocumentService22PortType echoSignStub;

	String echoSignURL;

	String echoSignTimeout;

	String url;

	private static class LazyHolder {
		private static final ESignatureProviderIfc echoSignService = new EchoSignService();
	}

	public static ESignatureProviderIfc getInstance() {
		return LazyHolder.echoSignService;
	}

	private EchoSignDocumentService22PortType getEchoSignStub(SessionContextIfc session) throws ApplicationException {
		EnvironmentIfc env = ConfigDatabase.getDb(session, "contractgen");
		String urlString = env.getProperty("contractgen.esignature_url");
		String timeout = env.getProperty("contractgen.esignature_timeout");

		if ((echoSignStub == null) || (!echoSignURL.equals(urlString)) || (!echoSignTimeout.equals(timeout))) {
			int t;
			try {
				t = Integer.parseInt(timeout);
			} catch (NumberFormatException e) {
				t = 300000;
			}

			if ((urlString == null) || (!StringUtil.hasValue(urlString))) {
				ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.noconfiguration");

				throw ae;
			}

			URL url = null;
			try {
				url = new URL(urlString + "?WSDL");
			} catch (MalformedURLException e) {
				Logger.getLogger(EchoSignDocumentService22.class.getName()).log(Level.INFO, "Can not initialize the default wsdl from {0}", urlString);
			}

			EchoSignDocumentService22 ss = new EchoSignDocumentService22(url, ECHOSIGN_SERVICENAME);
			echoSignStub = ss.getEchoSignDocumentService22HttpPort();

			Map<String, Object> requestContext = ((BindingProvider) echoSignStub).getRequestContext();
			requestContext.put("javax.xml.ws.client.receiveTimeout", Integer.valueOf(t));

			echoSignURL = urlString;
			echoSignTimeout = timeout;
		}

		return echoSignStub;
	}

	private void moveUser(SessionContextIfc session, String apiKey, String groupKey) throws ApplicationException {
		try {
			EchoSignDocumentService22PortType stub = getEchoSignStub(session);
			MoveUsersToGroup moveUser = new MoveUsersToGroup();
			moveUser.setApiKey(apiKey);
			moveUser.setGroupKey(groupKey);

			ArrayOfString userEmail = new ArrayOfString();
			userEmail.getStrings().add(session.getAccount().getEmail());

			UsersToMoveInfo users = new UsersToMoveInfo();
			users.setUserEmails(userEmail);
			moveUser.setUsersToMoveInfo(users);

			stub.moveUsersToGroup(moveUser);

		} catch (Exception ex) {
			Log.error(session.createLogMessage().setLogMessage("Error when moving echosign user account").setException(ex).setClass(getClass().getName()).setMethod("moveUser"));

			ApplicationException ae = new ApplicationException(session, "zcustom", "exception.doccommon.esignature.move_user_error");

			ae.setOriginalException(ex);
			ae.setMessageModifiers(new Object[] { ex.getMessage() });
			throw ae;
		}
	}

	private String createUser(SessionContextIfc session, String apiKey) throws ApplicationException {
		String userEmail = null;
		try {
			EchoSignDocumentService22PortType stub = getEchoSignStub(session);
			CreateUser createUser = new CreateUser();
			createUser.setApiKey(apiKey);

			UserCreationInfo userCreationInfo = new UserCreationInfo();
			userCreationInfo.setEmail(session.getAccount().getEmail());
			String generatedPassword = com.sap.odp.usermgmt.PasswordGenerator.generatePassword(0);

			userCreationInfo.setPassword(generatedPassword);
			userCreationInfo.setFirstName(session.getAccount().getFirstName());
			userCreationInfo.setLastName(session.getAccount().getLastName());
			userCreationInfo.setOptIn(com.sap.eso.webservice.echosign.api.dto.OptIn.NO);
			createUser.setUserInfo(userCreationInfo);

			CreateUserResponse response = stub.createUser(createUser);
			if (response.getUserKey() != null) {
				try {
					userEmail = session.getAccount().getEmail();

					MessagingComponent eMailer = new MessagingComponent(session);
					MessageBo msg = eMailer.newMessage();
					Properties mailProps = new Properties();

					msg.setMailType(new MailTypeEnum(189));
					msg.setRecipients(session.getUserAccount());
					msg.setFrom(session.getUserAccount());
					mailProps.setProperty("EMAIL", session.getAccount().getEmail());
					mailProps.setProperty("PASSWORD", generatedPassword);
					msg.setParamValues(mailProps);
					eMailer.sendMessage(msg);
				} catch (Exception ex) {
					Log.error(session.createLogMessage().setLogMessage("Error when sending email with echosign password to e-Sourcing user with email: " + session.getUserAccount().getEmail())
							.setException(ex).setClass(getClass().getName()).setMethod("createUser"));

					ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.send_echosign_email");

					ae.setOriginalException(ex);
					ae.setMessageModifiers(new Object[] { session.getUserAccount().getEmail() });
					throw ae;
				}

			}
		} catch (Exception ex) {
			Log.error(session.createLogMessage().setLogMessage("Error when creating echosign user account").setException(ex).setClass(getClass().getName()).setMethod("createUser"));

			ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.create_user_error");

			ae.setOriginalException(ex);
			ae.setMessageModifiers(new Object[] { ex.getMessage() });
			throw ae;
		}

		return userEmail;
	}

	public ESignatureDocumentStatusInfo getDocumentStatus(String documentKey, SessionContextIfc session) throws ApplicationException {
		ESignatureDocumentStatusInfo documentStatus = new ESignatureDocumentStatusInfo();

		try {
			EchoSignDocumentService22PortType stub = getEchoSignStub(session);
			GetDocumentInfo documentInfo = new GetDocumentInfo();
			documentInfo.setApiKey(getApiKey(session));
			documentInfo.setDocumentKey(documentKey);

			GetDocumentInfoResponse response = stub.getDocumentInfo(documentInfo);
			DocumentInfo documentInfoResponse = response.getDocumentInfo();
			documentStatus.setDocumentName(documentInfoResponse.getName());
			documentStatus.setMessage(documentInfoResponse.getMessage());

			AgreementStatus status = documentInfoResponse.getStatus();

			if (status.equals(AgreementStatus.OUT_FOR_SIGNATURE)) {
				documentStatus.setDocumentStatus(2);
			} else if (status.equals(AgreementStatus.SIGNED)) {
				documentStatus.setDocumentStatus(3);
			} else if (status.equals(AgreementStatus.ABORTED)) {
				documentStatus.setDocumentStatus(4);
			} else if (status.equals(AgreementStatus.EXPIRED)) {
				documentStatus.setDocumentStatus(5);
			} else if (status.equals(AgreementStatus.ARCHIVED)) {
				documentStatus.setDocumentStatus(6);
			} else if (status.equals(AgreementStatus.AUTHORING)) {
				documentStatus.setDocumentStatus(19);
			} else {
				documentStatus.setDocumentStatus(8);
			}
			List<DocumentHistoryEvent> documentHistories = documentInfoResponse.getEvents().getDocumentHistoryEvents();
			ESignatureDocumentHistory[] dsDocumentHistories = new ESignatureDocumentHistory[documentHistories.size()];
			for (int i = 0; i < documentHistories.size(); i++) {
				DocumentHistoryEvent documentHistory = (DocumentHistoryEvent) documentHistories.get(i);
				dsDocumentHistories[i] = ESignatureDocumentHistory.newInstance();
				dsDocumentHistories[i].setDate(documentHistory.getDate().toGregorianCalendar().getTime());
				dsDocumentHistories[i].setDescription(documentHistory.getDescription());
			}
			documentStatus.setDocumentHistory(dsDocumentHistories);
		} catch (Exception e) {
			if ((e instanceof ApplicationException)) {
				throw ((ApplicationException) e);
			}

			try {
				ping(session);
			} catch (Exception ex) {
				Log.error(session.createLogMessage("Unable to ping EchoSign").setException(ex).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

				ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.connect_to_echosign");

				ae.initCause(new UnknownHostException());
				ae.setOriginalException(ex);
				throw ae;
			}

			Log.error(
					session.createLogMessage("Unable to retrieve the document status from EchoSign").setException(e).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

			ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.retrieve_document_info");

			ae.setOriginalException(e);
			ae.setMessageModifiers(new Object[] { e.getMessage() });
			throw ae;
		}

		return documentStatus;
	}

	public Attachment getSignedDocument(String documentKey, String fileName, SessionContextIfc session) throws ApplicationException {
		try {
			EchoSignDocumentService22PortType stub = getEchoSignStub(session);
			GetLatestDocument latestDocument = new GetLatestDocument();
			latestDocument.setApiKey(getApiKey(session));
			latestDocument.setDocumentKey(documentKey);

			GetLatestDocumentResponse response = stub.getLatestDocument(latestDocument);

			int dotIndex = fileName.lastIndexOf('.');
			if (dotIndex >= 0) {
				fileName = fileName.substring(0, dotIndex) + ".pdf";
			} else {
				fileName = fileName + ".pdf";
			}
			byte[] dh = response.getPdf();
			ByteArrayInputStream in = new ByteArrayInputStream(dh);
			Attachment attachment = new Attachment();
			ParentObjectReference parent = new ParentObjectReference();
			attachment.setFileData(fileName, in, parent, session);

			return attachment;
		} catch (Exception e) {
			if ((e instanceof ApplicationException)) {
				throw ((ApplicationException) e);
			}
			Log.error(
					session.createLogMessage("Unable to retrieve the signed document from EchoSign.").setException(e).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

			throw new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.retrieve_signed_document");
		}
	}

	public static byte[] createByteArray(SessionContextIfc ctx, Attachment attachment) throws ApplicationException, com.sap.odp.api.common.exception.DatabaseException {
		byte[] data = null;

		File file = attachment.getFileData(ctx);
		if ((file == null) || (!file.exists())) {
			return new byte[0];
		}
		try {
			FileInputStream is = new FileInputStream(file);

			try {
				int nSize = is.available();

				data = new byte[nSize];

				int tempIn = is.read(data);
				if (tempIn != nSize) {
					Log.warning(ctx.createLogMessage().setLogMessage("Only read " + tempIn + " of " + nSize + " expected bytes").setClass("EchoSignService")
							.setMethod("createByteArray(SessionContextIfc, Attachment)"));
				}

			} finally {

				is.close();

				attachment.removeTmpFile();
			}
		} catch (IOException ex) {
			throw new ApplicationException(ctx, ex);
		}
		return data;
	}

	public String sendDocument(SubCollnBo signers, Attachment attachment, ESignatureTypeEnum signatureType, String contractDocumentDisplayName, String message, SessionContextIfc session)
			throws Exception {
		try {
			String apiKey = getApiKey(session);

			String userEmail = getUserEmail(session, apiKey);

			ArrayOfFileInfo fileInfos = new ArrayOfFileInfo();
			FileInfo fileInfo = new FileInfo();
			File file = attachment.getFileData(session);
			byte[] byteData = createByteArray(session, attachment);

			if (file != null) {
				fileInfo.setFile(byteData);
				fileInfo.setFileName(attachment.getDisplayName());
				fileInfos.getFileInfos().add(fileInfo);
			} else {
				Log.error(session.createLogMessage("Could not retrieve the attachement content.").setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

				throw new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.send_document");
			}

			Iterator<?> iter = signers.getIterator();
			ArrayOfRecipientInfo arryRecipientInfo = new ArrayOfRecipientInfo();
			List<RecipientInfo> recipientInfolist = arryRecipientInfo.getRecipientInfos();

			while (iter.hasNext()) {
				ESignatureSignerBo signerBo = (ESignatureSignerBo) iter.next();
				RecipientInfo recipientInfo = new RecipientInfo();
				recipientInfo.setEmail(signerBo.getSignerEmail());
				recipientInfo.setRole(RecipientRole.SIGNER);
				recipientInfo.setSigningOrder(signerBo.getSignerOrder());
				recipientInfolist.add(recipientInfo);
			}
			DocumentCreationInfo documentInfo = new DocumentCreationInfo();

			documentInfo.setRecipients(arryRecipientInfo);
			documentInfo.setName(contractDocumentDisplayName);
			documentInfo.setMessage(message);
			documentInfo.setFileInfos(fileInfos);
			documentInfo.setSignatureType(com.sap.eso.webservice.echosign.api.dto.SignatureType.ESIGN);
			documentInfo.setSignatureFlow(SignatureFlow.SENDER_SIGNATURE_NOT_REQUIRED);

			SendDocumentInteractive sDoc = new SendDocumentInteractive();
			sDoc.setApiKey(apiKey);
			sDoc.setDocumentCreationInfo(documentInfo);
			SenderInfo sender = new SenderInfo();
			sender.setEmail(userEmail);
			sDoc.setSenderInfo(sender);
			SendDocumentInteractiveOptions interOpt = new SendDocumentInteractiveOptions();
			interOpt.setAuthoringRequested(Boolean.valueOf(true));
			interOpt.setAutoLoginUser(Boolean.valueOf(true));
			interOpt.setNoChrome(Boolean.valueOf(false));
			sDoc.setSendDocumentInteractiveOptions(interOpt);

			EchoSignDocumentService22PortType stub = getEchoSignStub(session);
			SendDocumentInteractiveResponse response = stub.sendDocumentInteractive(sDoc);
			String key = null;
			if ((response != null) && (response.getSendDocumentInteractiveResult() != null) && (response.getSendDocumentInteractiveResult().isSuccess().booleanValue())) {
				SendDocumentInteractiveResult interactiveRes = response.getSendDocumentInteractiveResult();
				key = interactiveRes.getDocumentKey().getDocumentKey();
				setSenderViewUrl(interactiveRes.getUrl());
			} else {
				ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.send_document");

				String errorMsg = "Empty Response";
				if (response.getSendDocumentInteractiveResult() != null) {
					errorMsg = response.getSendDocumentInteractiveResult().getErrorMessage();
					if (StringUtil.isEmpty(errorMsg)) {
						errorMsg = response.getSendDocumentInteractiveResult().getErrorCode().toString();
					}
				}
				ae.setMessageModifiers(new Object[] { errorMsg });
				throw ae;
			}
			return key;
		} catch (Exception ex) {
			if ((ex instanceof ApplicationException)) {
				throw ex;
			}
			try {
				ping(session);
			} catch (Exception e) {
				Log.error(session.createLogMessage("Unable to ping EchoSign").setException(e).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

				ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.connect_to_echosign");

				ae.initCause(new UnknownHostException());
				throw ae;
			}

			Log.error(session.createLogMessage("Could not send contract document for signature.").setException(ex).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

			ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.send_document");

			ae.setMessageModifiers(new Object[] { ex.getMessage() });
			throw ae;
		}
	}

	private void setSenderViewUrl(String url) {
		this.url = url;
	}

	public String getSenderViewUrl() {
		return url;
	}

	private String getApiKey(SessionContextIfc session) throws ApplicationException {
		String apiKey = null;
		try {
			IntegratedSystemConfigHome configHome = new IntegratedSystemConfigHome(session);
			IntegratedSystemConfigBo configBo = (IntegratedSystemConfigBo) configHome.findActiveDoc(new IntegratedSystemTypeEnum(3));

			apiKey = (String) configBo.getPropertyValueById("API_KEY");
		} catch (Exception ex) {
			Log.error(session.createLogMessage("Unable to retrieve the api key from the ISC").setException(ex).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

			throw new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.get_api_key");
		}

		return apiKey;
	}

	private String getGroupKey(SessionContextIfc session) {
		return IapiSystemUtilities.getSystemProperty(session, "zcustom", "contractgen.esignature_CLM_groupID", "");
	}

	private String getUserEmail(SessionContextIfc session, String apiKey) throws Exception {
		try {
			EchoSignDocumentService22PortType stub = getEchoSignStub(session);

			GetUserInfo userInfo = new GetUserInfo();
			userInfo.setApiKey(apiKey);

			GetUserInfoOptions userInfoOptions = new GetUserInfoOptions();
			userInfoOptions.setEmail(session.getAccount().getEmail());

			userInfo.setOptions(userInfoOptions);

			GetUserInfoResponse response = stub.getUserInfo(userInfo);
			GetUserInfoResult result = response.getGetUserInfoResult();

			if (!result.isSuccess().booleanValue()) {
				if (GetUserInfoResultErrorCode.INVALID_EMAIL.equals(result.getErrorCode())) {
					// This is where our user doesn't exist and needs to be created
					return createUser(session, apiKey);
				} else if (GetUserInfoResultErrorCode.NO_PERMISSION.equals(result.getErrorCode())) {
					// User is sitting in a different Adobe account - throw error
					ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.create_envelope");
					ae.setMessageModifiers(new Object[] { "An user account for your email address exists in a different Adobe account. Please contact helpdesk." });
					throw ae;
				} else if (GetUserInfoResultErrorCode.INVALID_API_KEY.equals(result.getErrorCode())) {
					// Api Key errors
					ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.invalid_api_key");
					throw ae;
				} else {
					// Any other errors MISC & EXCEPTIONS
					ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.create_envelope");
					ae.setMessageModifiers(new Object[] { response.getGetUserInfoResult().getErrorCode().value() + " - " + response.getGetUserInfoResult().getErrorMessage() });
					throw ae;
				}
			}

			// At this stage the user exists in the adobe account
			String userEmail = result.getData().getEmail();

			// If the user is not part of the CLM group, move it there
			if (!"SAP CLM".equals(result.getData().getGroup())) {
				moveUser(session, apiKey, getGroupKey(session));
			}

			return userEmail;

		} catch (Exception ex) {
			if ((ex instanceof ApplicationException)) {
				throw ex;
			}

			try {
				ping(session);
			} catch (Exception e) {
				Log.error(session.createLogMessage("Unable to ping EchoSign").setException(e).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

				ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.connect_to_echosign");

				ae.initCause(new UnknownHostException());
				throw ae;
			}

			Log.error(session.createLogMessage("Unable to retrieve the user key from EchoSign").setException(ex).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

			throw new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.get_users_in_account");
		}
	}

	public void ping(SessionContextIfc session) throws ApplicationException, RemoteException {
		EchoSignDocumentService22PortType stub = getEchoSignStub(session);

		TestPing testPingIn = new TestPing();
		testPingIn.setApiKey(getApiKey(session));

		stub.testPing(testPingIn);
	}

	public void deleteDocument(String documentKey, SessionContextIfc session) throws ApplicationException {
		try {
			EchoSignDocumentService22PortType stub = getEchoSignStub(session);
			CancelDocument cancelDoc = new CancelDocument();
			cancelDoc.setDocumentKey(documentKey);
			cancelDoc.setApiKey(getApiKey(session));
			cancelDoc.setNotifySigner(true);
			cancelDoc.setComment("This signature request was voided");
			CancelDocumentResponse response = stub.cancelDocument(cancelDoc);
			if ((response != null) && (response.getCancelDocumentResult() != null)) {
				CancelDocumentResult result = response.getCancelDocumentResult();
				if (!Result.CANCELLED.equals(result.getResult())) {
					ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.delete_envelope");

					ae.setMessageModifiers(new Object[] { result.getResult().value() });
					throw ae;
				}
			}
		} catch (Exception ex) {
			if ((ex instanceof ApplicationException)) {
				throw ((ApplicationException) ex);
			}

			try {
				ping(session);
			} catch (Exception e) {
				Log.error(session.createLogMessage("Unable to ping EchoSign").setException(e).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

				ApplicationException ae = new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.connect_to_echosign");

				ae.initCause(new UnknownHostException());
				throw ae;
			}

			Log.error(session.createLogMessage("Unable to delete the document").setException(ex).setClass(ClassUtils.getClassName()).setMethod(ClassUtils.getCallingMethodName()));

			throw new ApplicationException(session, "eso.exception", "exception.doccommon.esignature.delete_envelope");
		}
	}
}